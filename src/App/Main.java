package App;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.fxml.JavaFXBuilderFactory;
import javafx.scene.Parent;
import javafx.scene.image.Image;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        FXMLLoader loader=new FXMLLoader();
        loader.setLocation(getClass().getResource("UI.fxml"));
        loader.setBuilderFactory(new JavaFXBuilderFactory());
        Parent root=loader.load();
        Controller controller=loader.getController();
        primaryStage.setTitle("Jump!");
        primaryStage.getIcons().add(new Image(getClass().getResource("icon.jpg").toExternalForm()));
        controller.Init(primaryStage,root);
        primaryStage.show();

    }


    public static void main(String[] args) {
        launch(args);
    }
}
