# JumpAssistJFX
微信跳一跳辅助工具 JavaFx版



## <span style="color:red">注意：</span>
* <span style="color:red">严禁用于商业用途！！！</span>
* <span style="color:red">仅供学习交流使用。</span>
* <span style="color:red">可以通过任意形式对代码进行修改和发布，但由此产生的后果由修改者承担，作者概不负责。</span>

### 忘说了一点
想用这个刷分的算了吧，肯定会被清的。。（

</div>

### 软件使用方法：
1. 用数据线连接手机与电脑
2. 手机启动开发者选项，选择USB调试，弹出允许USB调试对话框选择确定按钮
3. 打开软件，点击连接按钮
4. 如果显示已连接，可以打开跳一跳游戏界面，之后点击右面的跳一跳按钮，程序会自动执行后续操作

###  如果出现 java.io.IOException: Cannot run program "adb": CreateProcess error=2 
可以把adb文件夹中的文件放在System32文件夹下或者将adb添加进环境变量<br/>
在Linux系统下好像点连接按钮不管用，在程序里直接使能跳一跳按钮就行了，这里不想改了。

### 更新信息：
1. 修复了已知bug
2. 新增了几个bug

## 注意：可能已过期